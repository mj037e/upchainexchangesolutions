$(document).ready(function(){

});

$(window).on( "load", function() {

    $('#bankFraud li').on('click',select);

    //input file
    var fileTarget = $('.upload-hidden');
    fileTarget.on('change', function(){
        if(window.FileReader)  var filename = $(this)[0].files[0].name;
        else var filename = $(this).val().split('/').pop().split('\\').pop();
        $(this).parent('div').siblings().children('.upload-name').val(filename);
    });

});

function select(e){
    var target = '';
    if(e.currentTarget) target = $(this);
    else target = e;
    var selected = target.attr('data-title');
    target.siblings('li').removeClass('active')
    target.addClass('active');
    $('.board-body[data-value="'+selected+'"]').show();
    $('.board-body:not([data-value="'+selected+'"])').hide();
}