$(document).ready(function(){

});

$(window).on( "load", function() {
    tableDiv.scrollFunc({
        'boxName': '.scroll-div', //스크롤 시킬 contents를 감싸는 box의 classname 필수입력
        'tabelName': '.inner', //스크롤 시킬 contents의 classname필수입력
        'theadName': '.header', //고정시킬 head classname 필수입력
        'theadFixed' : false,//default false //thead가 있어야 실행됨.
        'scrollWidth' : '4px', //default 5px , scrollRail과 같이 사용
        'scrollbarColor' : '#c3c3c3', //default rgba(0,0,0,.2)
        'scrollbarBorderRadius' : '5px', //default 12px,
        'scrollRail' : false, //default false
        'scrollRailColor' : '#e5e5e5' //default rgba(0,0,0,.2)
    });
});

//emj - box안에서 스크롤 할때 사용.
var scrollContainer = {

    scrollBox : '',//
    bodyName : '',
    headName : '',
    scrollWidth : '', //default 5px , scrollRail과 같이 사용
    scrollbarColor : '', //default rgba(0,0,0,.2)
    scrollbarBorderRadius : '', //default 12px,
    scrollRailColor : '', //default rgba(0,0,0,.2),
    booleanData : [],
    scrollData : [],
    resetRepeat : function(a,b){
        $.each(a,function(i,v){
            b.scrollReset(i);
            if(i == (a.length -1) ) {
                b.booleanData = [];
                b.scrollData = [];
                scrollOption();
            }
        });
    },
    scrollbarTopBoolFunc : function(me,i){
        var that = this;
        if(that.booleanData[i].theadFixedBool) {//head를 고정시킬꺼라면 head의 높이를 ~으로
            if(me.find(that.headName).length) that.scrollData[i].scrollbarTop = parseInt(me.find(that.headName).css('height')); //thead가 0개가 아니라면 head의 높이 가져오기.
            else that.scrollData[i].scrollbarTop = 0; //head가 0개라면 thead의 높이를 0으로.
        } else that.scrollData[i].scrollbarTop = 0;
    },
    scrollBarPosition : function (me, i){// 스크롤바의 height 구하는.
        var that = this;
        that.scrollbarTopBoolFunc(me, i);
        that.booleanData[i].scrollBool = !(parseInt(that.scrollData[i].boxHeight) > parseInt(that.scrollData[i].tableHeight));
        if(that.booleanData[i].scrollBool) that.scrollData[i].scrollHeight = 'calc('+ Math.round(that.scrollData[i].boxHeight / that.scrollData[i].tableHeight * 100) + '%'+' - '+ that.scrollData[i].scrollbarTop +'px)';
        else that.scrollData[i].scrollHeight = that.scrollData[i].boxHeight;//box의 크기보다 table의 크기가 작다면 전체크기로.
        that.scrollData[i].scrollBarHeight = parseInt(me.children('.scrollBar').css('height'));
    },
    scrollBarMove : function(e){
        var that = this;
        $(e.me).children('.scrollBar').css({
            'top' : e.topBool ? e.travelDistance + e.top : e.top,
            'height' : e.height,
            'border-radius': e.borderRadius,
            MozBorderRadius: e.borderRadius,
            WebkitBorderRadius: e.borderRadius,
        });
    },
    scrollDataUpdateFunc : function(me,i , delta){
        var that = this;
        that.booleanData[i].maxBottom = that.scrollData[i].boxOverSize > (that.scrollData[i].boxScrollTop + delta) ? true : false;
        that.scrollData[i].boxScrollTop = parseInt(me.scrollTop()); //현재 스크롭top값 업데이트.
        that.scrollData[i].tableHeight = me.children(that.bodyName).css('height'); //높이값이 한번에 안들어와서 다시 업데이트.
    },
    boxScrollMove : function (event , me, scrollMoveTop, i){//, headName
        var that = this;
        event.preventDefault();
        $(me).stop().scrollTop($(me).scrollTop() + scrollMoveTop);
        if(that.booleanData[i].theadFixedBool){
            $(me).find(that.headName).stop().css({'top': $(me).scrollTop()});
        } },
    conditional : function (event , me, scrollMoveTop, i){//maxBottom, boxScrollTop ,theadFixedBool
        var that = this;
        if(scrollMoveTop > 0 && that.booleanData[i].maxBottom ) {
            that.boxScrollMove(event, me , scrollMoveTop , i )//down
        }else if(scrollMoveTop < 0 && that.scrollData[i].boxScrollTop !== 0) {
            that.boxScrollMove(event, me, scrollMoveTop , i); //up
        }
        event.preventDefault();/*kyc에서만 넣기. 다른곳에서 사용할때는 지워야함.*/
    },
    scrollReset : function(num){/*scroll값 reset*/
        if(!(num == undefined)){
            var that = this;
            var box = $("[data-scroll-num="+num+"]");

            that.booleanData[num].maxBottom = true;
            that.booleanData[num].topBool = false;
            that.booleanData[num].scrollBool = false;

            that.scrollData[num].boxHeight = parseInt(box.css('height'));// box크기 구하기.
            that.scrollData[num].tableHeight = parseInt(box.find(that.bodyName).css('height'));//contents 크기 구하기

            that.scrollData[num].boxOverSize = parseInt(that.scrollData[num].tableHeight)-parseInt(that.scrollData[num].boxHeight); // contents보다 box가 얼마나 큰지.

            //head값 설정.  if(that.booleanData[num].theadFixedBool) {//head가 fixed라면
            if(box.find(that.headName).length) that.scrollData[num].scrollbarTop = parseInt(box.find(that.headName).css('height')); //thead가 0개가 아니라면 head의 높이 가져오기.
            else that.scrollData[num].scrollbarTop = 0; //head가 0개라면 head의 높이를 0으로.
        } else that.scrollData[num].scrollbarTop = 0;//head가 fixed가 아니라면 head의 높이를 0으로

        //box의 scrollTop값. 움직인값  that.scrollData[num].boxScrollTop = that.scrollData[num].scrollbarTop;

        //box의 크기가 contens보다 크면 false. box의 크기보다 contents의 크기가 크면 true.
        that.booleanData[num].scrollBool = !( that.scrollData[num].boxHeight > that.scrollData[num].tableHeight );

        /*스크롤 높이 구하기.*/
        //box의 크기보다 contents의 크기가 크면 스크롤의 높이를  (boxHeight/tableHeight)*100-scrollbarTop 으로. box의 크기가 더 크다면 box의 높이로.
        if(that.booleanData[num].scrollBool) that.scrollData[num].scrollHeight = 'calc('+ Math.round( (that.scrollData[num].boxHeight / that.scrollData[num].tableHeight) * 100) + '%'+' - '+ that.scrollData[num].scrollbarTop +'px)';
        else that.scrollData[num].scrollHeight = that.scrollData[num].boxHeight - that.scrollData[num].scrollbarTop;//box의 크기보다 table의 크기가 작다면 전체크기로.
        that.scrollData[num].scrollBarHeight = that.scrollData[num].boxHeight - that.scrollData[num].scrollbarTop;

        that.scrollData[num].scrollNow = Math.round(((that.scrollData[num].boxScrollTop) /that.scrollData[num].boxOverSize)* 100);//
        that.scrollData[num].scrollBarNow = parseInt(that.scrollData[num].boxHeight) - parseInt(that.scrollData[num].scrollBarHeight) - that.scrollData[num].scrollbarTop; //스크로바의 높이값을 뺀 이동해야할 총거리.;
        that.scrollData[num].travelDistance = (that.scrollData[num].scrollBarNow*(that.scrollData[num].scrollNow/100))+that.scrollData[num].boxScrollTop; //이동할거리; //앞으로 이동할 거리.

        /*바꾼값으로 실행.*/
        //box위치 이동.
        box.stop().scrollTop(0);
        //head가 있다면 head 위치 이동
        if(that.booleanData[num].theadFixedBool) box.find(that.headName).stop().css({'top':0});
        //스크롤바가 있다면 scrollbar의 크기 위치 변경.
        if(that.scrollbarBool){
            box.stop().css({//scrollbar 위치이동
                'top' : that.scrollData[num].scrollbarTop,
                'height' : that.scrollData[num].scrollBarHeight,
                'border-radius': that.booleanData[num].scrollBool ? that.scrollbarBorderRadius : 0,
                MozBorderRadius: that.booleanData[num].scrollBool ? that.scrollbarBorderRadius : 0,
                WebkitBorderRadius: that.booleanData[num].scrollBool ? that.scrollbarBorderRadius : 0,
            });

        }
    },  scrollFunc : function(val){//scroll 실행 function.
        var that = this;
        that.scrollBox = val.boxName;
        that.bodyName = val.tabelName;
        that.headName = val.theadName;
        that.scrollWidth = val.scrollWidth === undefined ? '5px' : val.scrollWidth;
        that.scrollbarColor = val.scrollbarColor === undefined ? 'rgba(0,0,0,.2)' : val.scrollbarColor;
        that.scrollbarBorderRadius = val.scrollbarBorderRadius === undefined ? '12px' : val.scrollbarBorderRadius;
        that.scrollRailColor = val.scrollRailColor === undefined ? 'rgba(0,0,0,.2)' : val.scrollRailColor;

        var scrollBox = that.scrollBox;
        var bodyName = that.bodyName;
        var headName = that.headName;
        var scrollWidth = that.scrollWidth;
        var scrollbarColor = that.scrollbarColor;
        var scrollbarBorderRadius = that.scrollbarBorderRadius;
        var scrollRailColor = that.scrollRailColor;

        $.each($(scrollBox),function(i,v){

            that.booleanData.push(new Object());
            that.scrollData.push(new Object());

            /*booleanData 고정값*/
            that.booleanData[i].theadFixedBool = !(val.theadFixed === undefined) && val.theadFixed;//val.theadFixed값이 없거나 false면 true반환 //default false;
            that.booleanData[i].scrollbarBool = val.scrollbar === undefined || val.scrollbar;
            that.booleanData[i].scrollRailBool = !(val.scrollRail === undefined) && val.scrollRail;
            that.booleanData[i].scrollBool = false;
            that.booleanData[i].maxBottom = true;
            that.booleanData[i].topBool = false;

            /*scrollData 스크롤 될때마다 값 업데이트.*/
            that.scrollData[i].boxHeight = $(v).attr('data-height');
            that.scrollData[i].boxScrollTop = parseInt($(v).scrollTop());//box의 scrollTop값. 움직인값;
            that.scrollData[i].scrollbarTop = 0;
            that.scrollData[i].tableHeight = 0;
            that.scrollData[i].boxOverSize = 0;
            that.scrollData[i].scrollHeight = 0;
            that.scrollData[i].scrollBarHeight = 0;
            that.scrollData[i].scrollBarNow = 0;
            that.scrollData[i].scrollNow = 0;
            that.scrollData[i].travelDistance = 0;

            $(v).css({'display':'block'});
            $(v).attr('data-scroll-num',i);

            /*scrollBox에  position relative를 넣고 table속성 fixed주기 */
            $(v).css({
                'position': 'relative',
                'overflow':'hidden',
                'height': that.scrollData[i].boxHeight,
            });
            $(v).children(bodyName).css({ 'table-laout' : 'fixed'});

            /*head를 고정시켰을때 css 넣기*/
            if(that.booleanData[i].theadFixedBool) {
                $(v).find(headName).css({
                    'display' : 'table',
                    'position' : 'absolute',
                    'width' : '100%',
                    'table-laout' : 'fixed'
                });
            }

            /*scrollbar를 보일꺼라면*/
            if(that.booleanData[i].scrollbarBool){
                that.scrollbarTopBoolFunc($(v), i);
                var scrollBarEle = $('<div></div>')
                    .addClass('scrollBar')
                    .css({
                        'background-color': that.scrollbarColor,
                        'position': 'absolute',
                        'top': that.scrollData[i].scrollbarTop,
                        'right' : '0px',
                        'display': 'none',
                        'width' : that.scrollWidth,
                        zIndex: 99
                    });
                $(v).append(scrollBarEle);
            }  $(v).children(headName).css({ 'table-laout' : 'fixed'});

            /*scrollRail을 보일꺼라면*/
            if(that.booleanData[i].scrollRailBool){
                that.scrollbarTopBoolFunc($(v), i);
                var scrollRailEle = $('<div></div>')
                    .addClass('scrollRail')
                    .css({
                        'background-color': that.scrollRailColor,
                        'position': 'absolute',
                        'top': that.scrollData[i].scrollbarTop,
                        'right' : '0px',
                        'bottom' : -that.scrollData[i].boxOverSize,
                        'display': 'none',
                        'width' : that.scrollWidth,
                        'border-radius': '0',
                        MozBorderRadius: '0',
                        WebkitBorderRadius: '0',
                        zIndex: 98
                    });
                $(v).append(scrollRailEle);
            }

            /*slideBox에 over했을때.*/
            $(v).on('mouseenter',function(){
                // console.log('over');
                that.scrollbarTopBoolFunc($(v), i);
                if(that.booleanData[i].scrollbarBool) {
                    that.scrollData[i].tableHeight = parseInt($(v).children(bodyName).css('height'));
                    that.scrollBarPosition($(v), i);
                    that.scrollBarMove({
                        'me' : v,
                        'height' : that.scrollData[i].scrollHeight,
                        'top' : that.scrollData[i].scrollbarTop, //
                        'borderRadius' : that.booleanData[i].scrollBool ? val.scrollbarBorderRadius : 0,
                        'topBool' : that.booleanData[i].topBool ,
                        'travelDistance' : that.scrollData[i].travelDistance ,
                    });
                    $(v).children('.scrollBar').stop().fadeIn(100);
                }
                if(that.booleanData[i].scrollRailBool) {
                    $(v).children('.scrollRail').css({ 'top': parseInt($(this).scrollTop())+parseInt(that.scrollData[i].scrollbarTop)});
                    $(v).children('.scrollRail').stop().fadeIn(100);
                };
            });

            /*slideBox에 out했을때.*/
            $(v).on('mouseleave',function(){
                // console.log('out');
                if(that.booleanData[i].scrollbarBool) $(v).children('.scrollBar').stop().fadeOut(1000);
                if(that.booleanData[i].scrollRailBool) $(v).children('.scrollRail').stop().fadeOut(1500);
            });

            /*slideBox안에서 scroll했을때.*/
            $(v).on('mousewheel DOMMouseScroll wheel',function(e) {
                // console.log('scroll');
                if(that.booleanData[i].scrollBool){
                    var E = e.originalEvent;
                    var delta = 0;
                    that.scrollData[i].tableHeight = parseInt($(v).children(bodyName).css('height'));
                    that.scrollData[i].boxOverSize = that.scrollData[i].tableHeight-parseInt(that.scrollData[i].boxHeight); //table의 높이가 box보다 얼마나 큰지.
                    that.scrollbarTopBoolFunc($(v), i);
                    //박스안 스크롤 이동
                    if(that.booleanData[i].maxBottom || that.scrollData[i].boxScrollTop !== 0 ){
                        that.scrollBarPosition($(v), i);
                        that.scrollBarMove({
                            'me' : v,
                            'height' : that.scrollData[i].scrollHeight,
                            'top' : that.scrollData[i].scrollbarTop, //
                            'borderRadius' : that.booleanData[i].scrollBool ? val.scrollbarBorderRadius : 0,
                            'topBool' : that.booleanData[i].topBool ,
                            'travelDistance' : that.scrollData[i].travelDistance ,
                        });
                        if (E.wheelDelta) {//크롬
                            delta = -(E.wheelDelta / 3);
                            that.conditional(e, v, delta, i);
                            /*if문 밖으로 놓으면 안됨. if문 안과 밖에 실행속도로인해  delta값을 못가져오는 경우가 생김*/
                            that.scrollDataUpdateFunc($(v), i , delta);
                        }else if (!(E.detail)) {//파이어폭스
                            delta = E.deltaY * 15;
                            that.conditional(e, v, delta, i);
                            /*if문 밖으로 놓으면 안됨. if문 안과 밖에 실행속도로인해  delta값을 못가져오는 경우가 생김*/
                            that.scrollDataUpdateFunc($(v), i , delta);
                        }; }  //스크롤바 이동
                    if(that.booleanData[i].scrollbarBool){
                        that.booleanData[i].topBool = true; //스크롤 한적이 있는지 여부.
                        that.scrollData[i].scrollNow = Math.round(((that.scrollData[i].boxScrollTop) /that.scrollData[i].boxOverSize)* 100); //현재 열마나 스크롤되었는지 %로 나타냄.
                        that.scrollData[i].scrollBarHeight =$(v).children('.scrollBar').css('height');
                        that.scrollData[i].scrollBarNow = parseInt(that.scrollData[i].boxHeight) - parseInt(that.scrollData[i].scrollBarHeight) - that.scrollData[i].scrollbarTop; //스크로바의 높이값을 뺀 이동해야할 총거리.
                        that.scrollData[i].travelDistance = (that.scrollData[i].scrollBarNow*(that.scrollData[i].scrollNow/100))+that.scrollData[i].boxScrollTop; //이동할거리
                        if (!that.booleanData[i].maxBottom) that.scrollData[i].travelDistance = that.scrollData[i].scrollBarNow+that.scrollData[i].boxScrollTop;// 박스안에서 아래로 스크롤이 끝나면 스크롤바가 끝에 가있도록 이동할거러의 값변경.
                        $(v).children('.scrollBar').css({'top': that.scrollData[i].travelDistance+that.scrollData[i].scrollbarTop});
                    }
                    if(that.booleanData[i].scrollRailBool) $(v).children('.scrollRail').stop().css({'top': parseInt($(this).scrollTop()+that.scrollData[i].scrollbarTop), 'bottom': -that.scrollData[i].boxOverSize });

                }
            });

        });
    },
}
var tableDiv = new Object(scrollContainer);
